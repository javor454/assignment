<?php

declare(strict_types=1);

namespace Assignment\Tests\DataProvider;

use Assignment\Infrastructure\Product\Factory;
use DateTimeImmutable;
use Generator;
use stdClass;

class ProductProvider
{
    private Factory $factory;

    public function __construct()
    {
        $this->factory = new Factory();
    }

    /**
     * @return \Generator
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function correctProductProvider(): Generator
    {
        foreach ($this->correctArrayProvider() as $key => $arr) {
            yield $key => $this->factory->createFromArray(reset($arr));
        }
    }

    public function correctArrayProvider(): Generator
    {
        yield 'product created in past' => [
            [
                'id' => 'P1',
                'name' => 'Product',
                'description' => 'Description',
                'createdAt' => [
                    'date' => '2020-01-01 00:00:00.000000',
                    'timezone' => '+00:00',
                ],
            ],
        ];
    }

    public function faultyArrayProvider(): Generator
    {
        yield 'empty array' => [
            [],
        ];

        yield 'missing key createdAt' => [
            [
                'id' => 'P1',
                'name' => 'Product',
                'description' => 'description',
            ],
        ];

        yield 'createdAt without timezone' => [
            [
                'id' => 'P1',
                'name' => 'Product',
                'description' => 'description',
                'createdAt' => [
                    'date' => '2020-01-01 00:00:00.000000',
                ],
            ],
        ];

        yield 'id not string' => [
            [
                'id' => 1,
                'name' => 'Product',
                'description' => 'description',
                'createdAt' => null,
            ],
        ];

        yield 'name not string' => [
            [
                'id' => 'P1',
                'name' => 4.4,
                'description' => 'description',
                'createdAt' => null,
            ],
        ];

        yield 'description not string' => [
            [
                'id' => 'P1',
                'name' => 'Product',
                'description' => new stdClass(),
                'createdAt' => null,
            ],
        ];
    }

    public function datetimeGreaterThanCreatedAtArrayProvider(): Generator
    {
        yield 'datetime created before instance of product' => [
            new DateTimeImmutable(),
            [
                'id' => 'P1',
                'name' => 'Product',
                'description' => 'Description',
                'createdAt' => null
            ],
        ];
    }
}
