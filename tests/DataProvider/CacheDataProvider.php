<?php

declare(strict_types=1);

namespace Assignment\Tests\DataProvider;

use DateInterval;
use Generator;
use stdClass;

class CacheDataProvider
{
    private ProductProvider $productProvider;

    public function __construct()
    {
        $this->productProvider = new ProductProvider();
    }

    /**
     * @return \Generator
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function correctDataProvider(): Generator
    {
        yield 'string' => [
            'a',
            'abc',
            null,
        ];

        yield 'positive int' => [
            'b',
            123,
            new DateInterval('PT1000000S'),
        ];

        yield 'negative int' => [
            'c',
            -123,
            new DateInterval('PT420S'),
        ];

        yield 'zero int' => [
            'd',
            -123,
            new DateInterval('PT420S'),
        ];

        yield 'positive float' => [
            'e',
            1.23,
            null,
        ];

        yield 'negative float' => [
            'f',
            -1.23,
            new DateInterval('PT10000S'),
        ];

        yield 'zero float' => [
            'g',
            0.0,
            null,
        ];

        yield 'true' => [
            'h',
            true,
            new DateInterval('PT40000S'),
        ];

        yield 'false' => [
            'ch',
            true,
            null,
        ];

        yield 'empty array' => [
            'j',
            [],
            null,
        ];

        yield 'array with value' => [
            'k',
            [1],
            null,
        ];

        yield 'associative array' => [
            'l',
            ['a' => 1],
            null,
        ];

        yield 'multidimensional array with value' => [
            'm',
            [[1]],
            null,
        ];

        yield 'object' => [
            'n',
            new stdClass(),
            null,
        ];

        yield 'key pattern' => [
            'azAZ09_.',
            new stdClass(),
            null,
        ];

        foreach ($this->productProvider->correctProductProvider() as $key => $product) {
            yield "product no: $key" => [
                $key,
                $product,
                null,
            ];
        }
    }

    public function faultySetDataProvider(): Generator
    {
        yield 'closure' => [
            '1',
            static function () { },
            null
        ];

        yield 'closure in array' => [
            '2',
            [static function () { }],
            new DateInterval('PT10000S'),
        ];
    }

    public function faultyGetDataProvider(): Generator
    {
        yield 'null' => [
            'i',
            null,
            null,
        ];
    }
}
