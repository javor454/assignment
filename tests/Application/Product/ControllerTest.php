<?php

declare(strict_types=1);

namespace Assignment\Tests\Application\Product;

use Assignment\Application\Product\Controller;
use Assignment\Infrastructure\Cache\Cache;
use Assignment\Infrastructure\Driver\MySQLDriver;
use Assignment\Domain\Product\Facade;
use Assignment\Infrastructure\Product\Factory;
use Assignment\Infrastructure\Product\Mapper;
use Assignment\Infrastructure\Product\Repository;
use Assignment\Infrastructure\Product\SearchesRepository;
use Assignment\Infrastructure\Storage\FileStorage;
use PHPUnit\Framework\TestCase;

class ControllerTest extends TestCase
{
    private static Controller $controller;

    private static FileStorage $cacheStorage;

    private static FileStorage $searchesStorage;

    /** Testcase setup */

    /** @throws \Assignment\Domain\Exception\InvalidArgumentException */
    public static function setUpBeforeClass(): void
    {
        self::$cacheStorage = new FileStorage(__DIR__ . '/../../../tmp/cache', 'productCache');
        self::$searchesStorage = new FileStorage(__DIR__ . '/../../../tmp/cache', 'productSearches');
        self::$controller = new Controller(
            new Facade(
                new Repository(
                    new Mapper(
                        new MySQLDriver(),
                        new Factory()
                    ),
                    new Cache(self::$cacheStorage)
                ),
                new SearchesRepository(self::$searchesStorage)
            )
        );
    }

    public static function tearDownAfterClass(): void
    {
        @unlink(self::$cacheStorage->getStorageFilePath());
        @unlink(self::$searchesStorage->getStorageFilePath());
    }

    /** Controller tests */

    /** @throws \Assignment\Domain\Exception\InvalidArgumentException */
    public function testGetProductSuccess(): void
    {
        self::assertJson(self::$controller->detail('P1'));
    }

    /** @throws \Assignment\Domain\Exception\InvalidArgumentException */
    public function testGetProductSearches(): void
    {
        self::assertIsString(self::$controller->searches());
    }
}
