<?php

declare(strict_types=1);

namespace Assignment\Tests\Infrastructure\Cache;

use Assignment\Infrastructure\Cache\Cache;
use Assignment\Infrastructure\Storage\FileStorage;
use Assignment\Infrastructure\Storage\MalformedDataException;
use PHPUnit\Framework\TestCase;
use Psr\SimpleCache\InvalidArgumentException;

class CacheTest extends TestCase
{
    private static Cache $cache;

    private static FileStorage $storage;

    /** Testcase setup */

    /** @throws \Assignment\Domain\Exception\InvalidArgumentException */
    public static function setUpBeforeClass(): void
    {
        self::$storage = new FileStorage(__DIR__ . '/../../../tmp/cache', 'test');
        self::$cache = new Cache(self::$storage);
    }

    public static function tearDownAfterClass(): void
    {
        @unlink(self::$storage->getStorageFilePath());
    }

    protected function tearDown(): void
    {
        self::$cache->clear();
    }

    /** Storage file and directory tests */

    public function testStorageFileExist(): void
    {
        self::assertFileExists(self::$storage->getStorageFilePath());
    }

    public function testStorageFileWritable(): void
    {
        self::assertFileIsWritable(self::$storage->getStorageFilePath());
    }

    public function testCacheDirExist(): void
    {
        self::assertDirectoryExists(self::$storage->getCacheDir());
    }

    public function testCacheDirWritable(): void
    {
        self::assertDirectoryIsWritable(self::$storage->getCacheDir());
    }

    /** Caching tests */

    /**
     * @dataProvider \Assignment\Tests\DataProvider\CacheDataProvider::correctDataProvider()
     *
     * @param string $key
     * @param mixed $value
     * @param mixed $ttl
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @noinspection PhpMissingParamTypeInspection
     */
    public function testCacheSuccess($key, $value, $ttl): void
    {
        self::$cache->set($key, $value, $ttl);
        $ex = self::$cache->get($key);

        self::assertEquals($value, $ex);
    }

    /**
     * @dataProvider \Assignment\Tests\DataProvider\CacheDataProvider::faultySetDataProvider()
     *
     * @param string $key
     * @param mixed $value
     * @param mixed $ttl
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @noinspection PhpMissingParamTypeInspection
     */
    public function testCacheSetFailure($key, $value, $ttl): void
    {
        $this->expectException(InvalidArgumentException::class);

        self::$cache->set($key, $value, $ttl);
        self::$cache->get($key);
    }

    /**
     * @dataProvider \Assignment\Tests\DataProvider\CacheDataProvider::faultyGetDataProvider()
     *
     * @param string $key
     * @param mixed $value
     * @param mixed $ttl
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @noinspection PhpMissingParamTypeInspection
     */
    public function testCacheGetFailure($key, $value, $ttl): void
    {
        $this->expectException(MalformedDataException::class);

        self::$cache->set($key, $value, $ttl);
        self::$cache->get($key);
    }
}
