<?php

declare(strict_types=1);

namespace Assignment\Tests\Infrastructure\Product;

use Assignment\Infrastructure\Cache\Cache;
use Assignment\Infrastructure\Driver\MySQLDriver;
use Assignment\Domain\Product\Facade;
use Assignment\Infrastructure\Product\Factory;
use Assignment\Infrastructure\Product\Mapper;
use Assignment\Infrastructure\Product\Repository;
use Assignment\Infrastructure\Product\SearchesRepository;
use Assignment\Infrastructure\Storage\FileStorage;
use PHPUnit\Framework\TestCase;

class FacadeTest extends TestCase
{
    private static Facade $facade;

    private static FileStorage $cacheStorage;

    private static FileStorage $searchesStorage;

    /** Testcase setup */

    /** @throws \Assignment\Domain\Exception\InvalidArgumentException */
    public static function setUpBeforeClass(): void
    {
        self::$cacheStorage = new FileStorage(__DIR__ . '/../../../tmp/cache', 'productCache');
        self::$searchesStorage = new FileStorage(__DIR__ . '/../../../tmp/cache', 'productSearches');
        self::$facade = new Facade(
            new Repository(
                new Mapper(
                    new MySQLDriver(),
                    new Factory()
                ),
                new Cache(self::$cacheStorage)
            ),
            new SearchesRepository(self::$searchesStorage)
        );
    }

    public static function tearDownAfterClass(): void
    {
        @unlink(self::$cacheStorage->getStorageFilePath());
        @unlink(self::$searchesStorage->getStorageFilePath());
    }

    /** @throws \Assignment\Infrastructure\Storage\FileNotFoundException */
    protected function tearDown(): void
    {
        self::$cacheStorage->clear();
        self::$searchesStorage->clear();
    }

    /** Facade tests */

    public function testGetProductSuccess(): void
    {
        $this->expectNotToPerformAssertions();

        self::$facade->getProduct('P1');
    }
}
