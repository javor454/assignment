<?php

declare(strict_types=1);

namespace Assignment\Tests\Infrastructure\Product;

use Assignment\Infrastructure\Cache\Cache;
use Assignment\Infrastructure\Driver\MySQLDriver;
use Assignment\Infrastructure\Product\Factory;
use Assignment\Infrastructure\Product\Mapper;
use Assignment\Infrastructure\Product\Repository;
use Assignment\Infrastructure\Storage\FileStorage;
use PHPUnit\Framework\TestCase;

class RepositoryTest extends TestCase
{
    private static Repository $repository;

    private static FileStorage $storage;

    /** Testcase setup */

    /** @throws \Assignment\Domain\Exception\InvalidArgumentException */
    public static function setUpBeforeClass(): void
    {
        self::$storage = new FileStorage(__DIR__ . '/../../../tmp/cache', 'test');
        self::$repository = new Repository(
            new Mapper(
                new MySQLDriver(),
                new Factory()
            ),
            new Cache(self::$storage)
        );
    }

    public static function tearDownAfterClass(): void
    {
        @unlink(self::$storage->getStorageFilePath());
    }

    /** @throws \Assignment\Infrastructure\Storage\FileNotFoundException */
    protected function tearDown(): void
    {
        self::$storage->clear();
    }

    /** Repository tests */

    /**
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function testGetProductSuccess(): void
    {
        $this->expectNotToPerformAssertions();

        self::$repository->getBy('P1');
    }
}
