<?php

declare(strict_types=1);

namespace Assignment\Tests\Infrastructure\Product;

use Assignment\Domain\Exception\InvalidArgumentException;
use Assignment\Infrastructure\Product\Factory;
use PHPUnit\Framework\TestCase;

class FactoryTest extends TestCase
{
    private static Factory $factory;

    /** Testcase setup */

    public static function setUpBeforeClass(): void
    {
        self::$factory = new Factory();
    }

    /** Factory tests */

    /**
     * @dataProvider \Assignment\Tests\DataProvider\ProductProvider::correctArrayProvider()
     *
     * @param array $product
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function testCreateFromArraySuccess(array $product): void
    {
        $this->expectNotToPerformAssertions();

        self::$factory->createFromArray($product);
    }

    /**
     * @dataProvider \Assignment\Tests\DataProvider\ProductProvider::faultyArrayProvider()
     *
     * @param array $product
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function testCreateFromArrayFailed(array $product): void
    {
        $this->expectException(InvalidArgumentException::class);

        self::$factory->createFromArray($product);
    }
}
