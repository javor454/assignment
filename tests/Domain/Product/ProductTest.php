<?php

declare(strict_types=1);

namespace Assignment\Tests\Domain\Product;

use Assignment\Infrastructure\Product\Factory;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{
    private static Factory $factory;

    /** Testcase setup */

    public static function setUpBeforeClass(): void
    {
        self::$factory = new Factory();
    }

    /** Product tests */

    /**
     * @dataProvider \Assignment\Tests\DataProvider\ProductProvider::datetimeGreaterThanCreatedAtArrayProvider()
     *
     * @param \DateTimeImmutable $now
     * @param array $productArr
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function testNowGreaterThanCreatedAt(DateTimeImmutable $now, array $productArr): void
    {
        $product = self::$factory->createFromArray($productArr);

        self::assertGreaterThan($now, $product->getCreatedAt());
    }

    /**
     * @dataProvider \Assignment\Tests\DataProvider\ProductProvider::correctArrayProvider()
     *
     * @param array $productArr
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     * @throws \JsonException
     */
    public function testJsonSerialize(array $productArr): void
    {
        $product = self::$factory->createFromArray($productArr);
        $json = json_encode($product, JSON_THROW_ON_ERROR);
        $array = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
        $new = self::$factory->createFromArray($array);

        self::assertEquals($product, $new);
    }

    /**
     * @dataProvider \Assignment\Tests\DataProvider\ProductProvider::correctArrayProvider()
     *
     * @param array $productArr
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function testSerializeUnserialize(array $productArr): void
    {
        $product = self::$factory->createFromArray($productArr);
        $deser = unserialize(serialize($product));

        self::assertEquals($deser, $product);
    }
}
