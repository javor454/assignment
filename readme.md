# BOOTIQ Product Controller
## Zadání
### Popis problému
Náš systém uchovává informace o jednotlivých produktech v ElasticSearch a MySQL databázi. Data jsou identická v obou databázích, primárně se dotazujeme ElasticSearch databáze, ale čas od času je třeba získávat data přímo z MySQL (třeba když provádíme různé experimenty s ElasticSeach) – v tuto chvíli samozřejmě musíme dát našim zákazníkům na front-endu zobrazovat data, takže je třeba přepnout na MySQL.

Vzhledem k tomu,že provoz na našich serverech je často extrémní, potřebujeme samozřejmě i data o produktech cachovat. Z počátku stačí cachovat do souboru, ale je třeba počítat s tím, že v budoucnu použijete nějakou pokročilejší technologii - a to snadno jen přepnutím v konfiguraci aplikace. Protože marketingové oddělení chce dlouhodobě sledovat zájem o jednotlivé produkty, je třeba také počítat počet dotazů na konkrétní produkty. Z počátku opět postačí ukládání v plain-textu, ale je třeba počítat s tím, že v budoucnu použijete pokročilejší technologii, nebo budete chtít změnit umístění těchto dat.

### Úkol
Vytvořte jednoduchou aplikaci, která bude poskytovat data o produktu prostřednictvím Controlleru ve formátu JSON.

Základní workflow:
- Je zavolán dotaz na produkt s jeho ID
- Jestliže je produkt zacachován, je vrácen z cache
- Jestliže produkt není v cache, dotážeme se ElasticSearch/MySQL databáze a výsledek uložíme do cache
- Počet dotazů na tento produkt zvýšíme o jedna
- Vrátíme data o produktu ve formátu JSON

Controller může vypadat takto:
```php
class ProductController
{
    /**
     * @param string $id
     * @return string
    */
    public function detail($id)
    {
        // do stuff and return json
    }
}
```

### Dodatečné informace
Máme drivery pro ElasticSearch i MySQL, takže je nemusíte psát. Po zavolání metody získáte data o produktu v poli. Drivery implentují následující interface:
```php
interface IElasticSearchDriver
{
    /**
     * @param string $id
     * @return array
    */
    public function findById($id);
}

interface IMySQLDriver
{
    /**
     * @param string $id
     * @return array
    */
    public function findProduct($id);
}
```
Náš Framework předává všechno, co potřebujete. Jestliže potřebujete předat nějaké parametry do konstruktoru controlleru, vždy vám předá sám ty správné (nemusíte se tedy o toto starat). Předáte-li jakékoliv ID produktu do driveru, vždy najde produkt – nemusíte se tedy zabývat výjimkami typu „Not found“. Cache je nekonečná – nemusíte se starat o její invalidaci. Jednou zacachovaná data nepotřebujeme nikdy mazat. Informace o počtech dotazů na produkt jsou jen jednoduché páry (ID Produktu) => (počet dotazů), žádná jiná data nejsou potřeba.

## Tests
- Test installation from root with `./vendor/bin/phpunit --version`
- Run from root with `./vendor/bin/phpunit tests`

## TODOs
### Optional
- `Infrastructure\Storage\FileStorage`
    - locking mechanism
    - implement the rest of psr-16 methods
- `Application`
    - add directories `\Api` a `\Cli`