<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Driver;

use Assignment\Domain\Driver\IDriver;
use Assignment\Domain\Driver\IElasticSearchDriver;

/** Dummy driver implementation for fetching products by id using Elastic Search. */
class ElasticSearchDriver implements IElasticSearchDriver, IDriver
{
    /**
     * Always finds dummy product with correct id.
     *
     * @param string $id
     * @return array
     */
    public function findById(string $id): array
    {
        return [
            'id' => $id,
            'name' => 'Product',
            'description' => 'Description',
            'createdAt' => [
                'date' => '2020-01-01 00:00:00.000000',
                'timezone' => '+00:00',
            ],
        ];
    }
}
