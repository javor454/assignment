<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Driver;

use Assignment\Domain\Driver\IDriver;
use Assignment\Domain\Driver\IMySQLDriver;

/** Dummy driver implementation for fetching products by id using MySQL. */
class MySQLDriver implements IMySQLDriver, IDriver
{
    /**
     * Always finds dummy product array with correct id.
     *
     * @param string $id
     * @return string[]
     */
    public function findProduct(string $id): array
    {
        return [
            'id' => $id,
            'name' => 'Product',
            'description' => 'Description',
            'createdAt' => [
                'date' => '2020-01-01 00:00:00.000000',
                'timezone' => '+00:00',
            ],
        ];
    }

    public function findById(string $id): array
    {
        return $this->findProduct($id);
    }
}
