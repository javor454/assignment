<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Storage;

use Assignment\Domain\Storage\StorageException;
use Throwable;

/** Thrown when unable to locate file, contains path to file. */
class FileNotFoundException extends StorageException
{
    public function __construct(string $file, Throwable $previous = null)
    {
        parent::__construct("File '$file' not found.", 0, $previous);
    }
}
