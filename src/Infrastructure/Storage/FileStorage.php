<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Storage;

use Assignment\Domain\Exception\InvalidArgumentException;
use Assignment\Domain\Exception\NotImplementedException;
use Assignment\Domain\Storage\IStorage;
use DateTimeImmutable;

/**
 * Able to store and retrieve serializable data to file under user-specified key.
 */
class FileStorage implements IStorage
{
    /** Unserialize options - allow all classes. */
    private const UNSER_OPTIONS = [true];
    /** Stream access mode for binary safe appending. */
    public const MODE_APPEND_BIN_SAFE = 'ab';
    /** Stream access mode for binary safe reading. */
    private const MODE_READ_BIN_SAFE = 'rb';
    /** Stream access mode for binary safe writing. */
    private const MODE_WRITE_BIN_SAFE = 'wb';
    private const KEY_KEY = 'key';
    private const KEY_VALUE = 'value';
    private const KEY_EXPIRATION = 'expiration';

    private string $filename;

    private string $cacheDir;

    /**
     * @param string $cacheDir
     * @param string $filename
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function __construct(string $cacheDir, string $filename)
    {
        if (!is_dir($cacheDir) || !is_writable($cacheDir)) {
            throw new InvalidArgumentException('Caching directory does not exist or is not writable.', ['cacheDir' => $cacheDir]);
        }
        $this->cacheDir = $cacheDir;

        $filePath = $cacheDir . DIRECTORY_SEPARATOR . $filename;
        if (!file_exists($filePath)) {
            touch($filePath);
        }
        $this->filename = $filename;
    }

    public function getCacheDir(): string
    {
        return $this->cacheDir;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     * @throws \Assignment\Infrastructure\Storage\FileNotFoundException
     * @throws \Assignment\Infrastructure\Storage\MalformedDataException
     */
    public function read(string $key, $default = null)
    {
        $stream = @fopen($this->getStorageFilePath(), self::MODE_READ_BIN_SAFE);
        if ($stream === false) {
            throw new FileNotFoundException($this->getStorageFilePath());
        }

        while ($line = fgets($stream)) {
            $data = unserialize($line, self::UNSER_OPTIONS);
            if (!isset($data[self::KEY_KEY], $data[self::KEY_VALUE]) || !array_key_exists(self::KEY_EXPIRATION, $data)) {
                throw new MalformedDataException($data);
            }

            $identifier = $data[self::KEY_KEY];
            if ($identifier !== $key) {
                continue;
            }

            $expiration = $data[self::KEY_EXPIRATION];
            if ($expiration !== null) {
                $now = new DateTimeImmutable();
                if ($expiration < $now) {
                    continue; // expired
                }
            }

            $default = $data[self::KEY_VALUE];

            break;
        }
        fclose($stream);

        return $default;
    }

    public function write(string $key, $value, ?DateTimeImmutable $expiration = null): bool
    {
        $data = [
            self::KEY_KEY => $key,
            self::KEY_VALUE => $value,
            self::KEY_EXPIRATION => $expiration,
        ];
        $line = serialize($data) . PHP_EOL;

        $stream = fopen($this->getStorageFilePath(), self::MODE_APPEND_BIN_SAFE);
        $result = (bool) fwrite($stream, $line);
        fclose($stream);

        return $result;
    }

    /**
     * @return bool
     * @throws \Assignment\Infrastructure\Storage\FileNotFoundException
     */
    public function clear(): bool
    {
        $stream = @fopen($this->getStorageFilePath(), self::MODE_WRITE_BIN_SAFE);
        if ($stream === false) {
            throw new FileNotFoundException($this->getStorageFilePath());
        }
        return fclose($stream);
    }

    /**
     * Returns storage file path.
     *
     * @return string
     */
    public function getStorageFilePath(): string
    {
        return $this->getCacheDir() . DIRECTORY_SEPARATOR . $this->getFilename();
    }

    /**
     * @param string $key
     * @return bool
     * @throws \Assignment\Infrastructure\Storage\FileNotFoundException
     * @throws \Assignment\Infrastructure\Storage\MalformedDataException
     */
    public function delete(string $key): bool
    {
        $stream = @fopen($this->getStorageFilePath(), self::MODE_READ_BIN_SAFE);
        if ($stream === false) {
            throw new FileNotFoundException($this->getStorageFilePath());
        }

        $persist = [];
        while ($line = fgets($stream)) {
            $data = unserialize($line, self::UNSER_OPTIONS);

            if (!isset($data[self::KEY_KEY], $data[self::KEY_VALUE]) || !array_key_exists(self::KEY_EXPIRATION, $data)) {
                throw new MalformedDataException($data);
            }

            $identifier = $data[self::KEY_KEY];
            if ($identifier !== $key) {
                $persist[$identifier] = $data[self::KEY_VALUE];
            }
        }
        fclose($stream);

        $this->clear();
        foreach ($persist as $identifier => $value) {
            $this->write($identifier, $value);
        }

        return true;
    }


    /**
     * Returns all saved values in key value pairs e.g. `['P1' => 123]`.
     *
     * @return iterable
     * @throws \Assignment\Infrastructure\Storage\FileNotFoundException
     * @throws \Assignment\Infrastructure\Storage\MalformedDataException
     */
    public function readAll(): iterable
    {
        $stream = @fopen($this->getStorageFilePath(), self::MODE_READ_BIN_SAFE);
        if ($stream === false) {
            throw new FileNotFoundException($this->getStorageFilePath());
        }

        while ($line = fgets($stream)) {
            $data = unserialize($line, self::UNSER_OPTIONS);

            if (!isset($data[self::KEY_KEY], $data[self::KEY_VALUE]) || !array_key_exists(self::KEY_EXPIRATION, $data)) {
                throw new MalformedDataException($data);
            }

            $expiration = $data[self::KEY_EXPIRATION];
            if ($expiration !== null) {
                $now = new DateTimeImmutable();
                if ($expiration < $now) {
                    continue; // expired
                }
            }

            yield $data[self::KEY_KEY] => $data[self::KEY_VALUE];
        }
        fclose($stream);
    }

    /**
     * @param string[] $keys
     * @param mixed $default
     * @return iterable
     * @throws \Assignment\Domain\Exception\NotImplementedException
     */
    public function readMultiple(iterable $keys, $default = null): iterable
    {
        throw new NotImplementedException(__METHOD__ . ' not implemented.');
    }

    /**
     * @param mixed[] $values
     * @param \DateTimeImmutable|null $expiration
     * @return bool
     * @throws \Assignment\Domain\Exception\NotImplementedException
     */
    public function writeMultiple(iterable $values, ?DateTimeImmutable $expiration): bool
    {
        throw new NotImplementedException(__METHOD__ . ' not implemented.');
    }

    /**
     * @param string[] $keys
     * @return bool
     * @throws \Assignment\Domain\Exception\NotImplementedException
     */
    public function deleteMultiple(iterable $keys): bool
    {
        throw new NotImplementedException(__METHOD__ . ' not implemented.');
    }

    /**
     * @param string $key
     * @return bool
     * @throws \Assignment\Domain\Exception\NotImplementedException
     */
    public function exists(string $key): bool
    {
        throw new NotImplementedException(__METHOD__ . ' not implemented.');
    }
}
