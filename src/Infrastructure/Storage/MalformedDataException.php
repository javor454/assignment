<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Storage;

use Assignment\Domain\Storage\StorageException;
use Throwable;

/** Thrown when storage encounters malformed data during CRUD. */
class MalformedDataException extends StorageException
{
    public function __construct($data, Throwable $previous = null)
    {
        parent::__construct('Malformed storage data: ' . var_export($data, true), 0, $previous);
    }
}
