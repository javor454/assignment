<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Cache;

use Assignment\Domain\Storage\IStorage;
use Closure;
use DateInterval;
use DateTimeImmutable;
use Psr\SimpleCache\CacheInterface;

/** Simple cache, complies with PSR-16. */
class Cache implements CacheInterface
{
    /** Represents cache miss - value which is returned in case found cache item is expired or not found. */
    public const MISS = null;
    /** Represents unlimited time to live. */
    public const NO_EXPIRATION = null;

    /** Represents character set, order and encoding requirements to comply with PSR. */
    private const KEY_FORMAT_PATTERN = '~[\w.]{1,64}~u';

    private IStorage $storage;

    public function __construct(IStorage $storage)
    {
        $this->storage = $storage;
    }

    public function get($key, $default = self::MISS)
    {
        $this->checkKeyFormat($key);

        return $this->storage->read($key, $default);
    }

    public function set($key, $value, $ttl = self::NO_EXPIRATION)
    {
        $this->checkKeyFormat($key);
        $this->checkValueSerializability($value);
        $ttlDateInterval = $this->convertTtlToDateInterval($ttl);

        $this->storage->write($key, $value, $this->getExpiration($ttlDateInterval));
    }

    public function delete($key)
    {
        $this->checkKeyFormat($key);

        $this->storage->delete($key);
    }

    public function clear(): bool
    {
        return $this->storage->clear();
    }


    public function getMultiple($keys, $default = self::MISS): iterable
    {
        foreach ($keys as $key) {
            $this->checkKeyFormat($key);
        }

        return $this->storage->readMultiple($keys, $default);
    }

    public function setMultiple($values, $ttl = self::NO_EXPIRATION): bool
    {
        foreach ($values as $value) {
            $this->checkValueSerializability($value);
        }

        return $this->storage->writeMultiple($values, $ttl);
    }

    public function deleteMultiple($keys): bool
    {
        foreach ($keys as $key) {
            $this->checkKeyFormat($key);
        }

        return $this->storage->deleteMultiple($keys);
    }

    public function has($key): bool
    {
        $this->checkKeyFormat($key);

        return $this->storage->exists($key);
    }

    /**
     * Returns the actual time when an item is set to go stale.
     *
     * @param \DateInterval|null $ttl
     * @return \DateTimeImmutable|null The actual time when an item is set to go stale or null for no expiration time.
     */
    public function getExpiration(?DateInterval $ttl): ?DateTimeImmutable
    {
        if ($ttl === null) {
            return self::NO_EXPIRATION;
        }

        return (new DateTimeImmutable())->add($ttl);
    }

    /**
     * Checks if cache keys type, format and encoding complies with minimal PSR-16 requirements.
     *
     * @param mixed $key
     * @throws \Assignment\Infrastructure\Cache\InvalidArgumentException
     */
    private function checkKeyFormat($key): void
    {
        if (
            !is_string($key)
            || !((bool) preg_match(self::KEY_FORMAT_PATTERN, $key))
        ) {
            throw new InvalidArgumentException('Cache key does not meet type or format requirements.', ['key' => $key]);
        }
    }

    /**
     * Checks if data complies with PSR-16 data type requirements.
     *
     * @param mixed $value
     * @throws \Assignment\Infrastructure\Cache\InvalidArgumentException
     */
    private function checkValueSerializability($value): void
    {
        $input = [$value];

        array_walk_recursive(
            $input,
            static function ($element) use ($value) {
                if (is_object($element) && $element instanceof Closure) {
                    throw new InvalidArgumentException('Unable to cache value because it contains Closure.', ['value' => $value]);
                }
            }
        );
    }

    /**
     * Converts valid ttl to DateInterval, null is also valid.
     *
     * @param mixed $ttl
     * @return \DateInterval|null
     * @throws \Assignment\Infrastructure\Cache\InvalidArgumentException
     */
    private function convertTtlToDateInterval($ttl): ?DateInterval
    {
        if ($ttl === self::NO_EXPIRATION || $ttl instanceof DateInterval) {
            return $ttl;
        }

        if (is_int($ttl) && $ttl > 0) {
            return new DateInterval("PT{$ttl}S");
        }

        throw new InvalidArgumentException("Time to live must be either int (seconds), DateInterval, or null (on expiration).", ['ttl' => $ttl]);
    }
}
