<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Cache;

/** Exception interface for invalid cache arguments. */
class InvalidArgumentException extends \Assignment\Domain\Exception\InvalidArgumentException implements \Psr\SimpleCache\InvalidArgumentException
{
}
