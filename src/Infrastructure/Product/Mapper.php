<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Product;

use Assignment\Domain\Driver\IDriver;
use Assignment\Domain\Product\Product;

/** Maps driver array data onto product entities. */
class Mapper
{
    private IDriver $driver;

    private Factory $factory;

    public function __construct(IDriver $driver, Factory $factory)
    {
        $this->driver = $driver;
        $this->factory = $factory;
    }

    /**
     * Returns product array by it's unique ID, creates instance using product factory.
     *
     * @param string $id
     * @return \Assignment\Domain\Product\Product
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function getProduct(string $id): Product
    {
        $arr = $this->driver->findById($id);

        return $this->factory->createFromArray($arr);
    }
}
