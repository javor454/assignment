<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Product;

use Assignment\Domain\Product\ISearchesRepository;
use Assignment\Domain\Storage\IStorage;

/** Tracks count of queries for specified product. */
class SearchesRepository implements ISearchesRepository
{
    private IStorage $storage;

    public function __construct(IStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Increments count of queries for specified product.
     *
     * @param string $productId Unique product identifier.
     */
    public function incrementSearches(string $productId): void
    {
        $searches = $this->storage->read($productId);

        if ($searches === null) {
            $this->storage->write($productId, 1);
        } else {
            $this->storage->delete($productId);
            $this->storage->write($productId, ++$searches);
        }
    }

    /**
     * Returns all searches pairs e.g. `['productId' => 123]`.
     *
     * @return int[]
     */
    public function getAll(): iterable
    {
        return $this->storage->readAll();
    }
}
