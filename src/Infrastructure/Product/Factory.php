<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Product;

use Assignment\Domain\Exception\InvalidArgumentException;
use Assignment\Domain\Product\Product;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use TypeError;

/** Unifies the location for instantiating the Product class. */
class Factory
{
    /**
     * Creates Product instance with values supplied by array.
     *
     * @param array $product
     * @return \Assignment\Domain\Product\Product
     * @throws \Assignment\Domain\Exception\InvalidArgumentException in case of invalid supplied data
     */
    public function createFromArray(array $product): Product
    {
        if (
            !isset($product['id'], $product['name'], $product['description'])
            || !array_key_exists('createdAt', $product)
        ) {
            throw new InvalidArgumentException('Invalid data passed to product factory.', ['product' => $product]);
        }

        if ($product['createdAt'] !== null) {
            try {
                $createdAt = new DateTimeImmutable($product['createdAt']['date'], new DateTimeZone($product['createdAt']['timezone']));
            } catch (Exception $e) {
                throw new InvalidArgumentException('Invalid datetime format.', ['createdAt' => $product['createdAt']]);
            }
        } else {
            $createdAt = null;
        }

        try {
            return new Product(
                $product['id'],
                $product['name'],
                $product['description'],
                $createdAt
            );
        } catch (TypeError $e) {
            throw new InvalidArgumentException($e->getMessage(), ['data' => $product]);
        }
    }
}
