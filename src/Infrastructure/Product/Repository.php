<?php

declare(strict_types=1);

namespace Assignment\Infrastructure\Product;

use Assignment\Domain\Product\IRepository;
use Assignment\Domain\Product\Product;
use Psr\SimpleCache\CacheInterface;

/** Queries mapper for product entities, which are cached. */
class Repository implements IRepository
{
    private const CACHE_NO_EXPIRATION = null;
    private const CACHE_MISS = null;

    private Mapper $mapper;

    private CacheInterface $cache;

    public function __construct(Mapper $mapper, CacheInterface $cache)
    {
        $this->mapper = $mapper;
        $this->cache = $cache;
    }

    /**
     * Tries to load product from cache. In case of cache miss, mapper is queried and data are stored in cache before they're returned.
     *
     * @param string $id
     * @return \Assignment\Domain\Product\Product
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getBy(string $id): Product
    {
        $product = $this->cache->get($id);
        if ($product === self::CACHE_MISS) {
            $product = $this->mapper->getProduct($id);
            $this->cache->set($id, $product, self::CACHE_NO_EXPIRATION);
        }

        return $product;
    }
}
