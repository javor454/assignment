<?php

declare(strict_types=1);

namespace Assignment\Application\Product;

use Assignment\Domain\Response\IResponse;
use Assignment\Domain\Response\JsonResponse;
use Assignment\Domain\Product\Facade;
use Assignment\Domain\Response\TextResponse;
use Assignment\Domain\Util\StringHelper;

/** Serves product related resources. */
class Controller
{
    private Facade $facade;

    public function __construct(Facade $facade)
    {
        $this->facade = $facade;
    }

    /**
     * Returns product detail in JSON.
     *
     * @param string $id
     * @return string
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function detail(string $id): string
    {
        $product = $this->facade->getProduct($id);

        return $this->render(new JsonResponse($product));
    }

    /**
     * Returns search counts per all searched product IDs.
     *
     * @return string
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function searches(): string
    {
        $searches = $this->facade->getAllProductSearches();

        return $this->render(new TextResponse($searches, $this->getSearchesFormatFun()));
    }

    /**
     * Formats searches data to string.
     *
     * @return callable
     */
    private function getSearchesFormatFun(): callable
    {
        return static function (iterable $data) {
            $text = StringHelper::addLine('Product searches: ');
            foreach ($data as $productId => $searches) {
                $text .= StringHelper::addLine(sprintf("ID: '%s'   Searches: %d time(s)", $productId, $searches));
            }

            return $text;
        };
    }

    /**
     * Renders responses.
     *
     * @param \Assignment\Domain\Response\IResponse $response
     * @return string
     */
    private function render(IResponse $response): string
    {
        return $response->render();
    }
}
