<?php

declare(strict_types=1);

namespace Assignment\Domain\Product;

/** Product searches repository interface tracks count of queries for specified product and enforces this functionality across all infrastructure implementations. */
interface ISearchesRepository
{
    /**
     * Increments count of queries for specified product.
     *
     * @param string $productId Unique product identifier.
     */
    public function incrementSearches(string $productId): void;

    /**
     * Returns all searches pairs e.g. `['productId' => 123]`.
     *
     * @return iterable
     */
    public function getAll(): iterable;
}
