<?php

declare(strict_types=1);

namespace Assignment\Domain\Product;

use Assignment\Domain\Exception\InvalidArgumentException;
use DateTimeImmutable;
use DateTimeZone;
use Exception;
use JsonException;
use JsonSerializable;
use Serializable;

/** Product entity describes products identity, name, description and date of creation. Is serializable. */
class Product implements JsonSerializable, Serializable
{
    private string $id;

    private string $name;

    private string $description;

    private DateTimeImmutable $createdAt;

    public function __construct(string $id, string $name, string $description, ?DateTimeImmutable $createdAt = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;

        if ($createdAt === null) {
            $this->createdAt = new DateTimeImmutable();
        } else {
            $this->createdAt = $createdAt;
        }
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'createdAt' => $this->getCreatedAt(),
        ];
    }

    /**
     * @return string
     * @throws \JsonException
     */
    public function serialize(): string
    {
        return json_encode($this, JSON_THROW_ON_ERROR);
    }

    /**
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     * @noinspection PhpDocSignatureInspection
     */
    public function unserialize($serialized): void
    {
        try {
            $product = json_decode($serialized, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new InvalidArgumentException('Serialized data not valid.', ['serialized' => $serialized]);
        }

        if (!isset(
            $product['id'],
            $product['name'],
            $product['description'],
            $product['createdAt']['date'],
            $product['createdAt']['timezone']
        )) {
            throw new InvalidArgumentException('Deserialized data not valid.', ['product' => $product]);
        }

        try {
            $createdAt = new DateTimeImmutable($product['createdAt']['date'], new DateTimeZone($product['createdAt']['timezone']));
        } catch (Exception $e) {
            throw new InvalidArgumentException('Invalid datetime format.', ['createdAt' => $product['createdAt']]);
        }

        $this->id = $product['id'];
        $this->name = $product['name'];
        $this->description = $product['description'];
        $this->createdAt = $createdAt;
    }
}
