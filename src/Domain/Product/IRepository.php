<?php

declare(strict_types=1);

namespace Assignment\Domain\Product;

/** Product repository interface enforces functionality across all infrastructure implementations.  */
interface IRepository
{
    /**
     * @param string $id
     * @return Product
     */
    public function getBy(string $id): Product;
}