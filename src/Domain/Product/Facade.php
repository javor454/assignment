<?php

declare(strict_types=1);

namespace Assignment\Domain\Product;

/** Simplifies access to product related resources. */
class Facade
{
    private IRepository $repository;

    private ISearchesRepository $searchesRepository;

    public function __construct(IRepository $repository, ISearchesRepository $searchesRepository)
    {
        $this->repository = $repository;
        $this->searchesRepository = $searchesRepository;
    }

    /**
     * Returns product, tracks count of queries for specified product.
     *
     * @param string $id
     * @return \Assignment\Domain\Product\Product
     */
    public function getProduct(string $id): Product
    {
        $product = $this->repository->getBy($id);
        $this->searchesRepository->incrementSearches($id);

        return $product;
    }

    /**
     * Returns count of searches per product ID.
     *
     * @return iterable
     */
    public function getAllProductSearches(): iterable
    {
        return $this->searchesRepository->getAll();
    }
}
