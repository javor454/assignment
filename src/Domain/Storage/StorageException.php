<?php

declare(strict_types=1);

namespace Assignment\Domain\Storage;

use Assignment\Domain\Exception\BaseException;

/** Base exception for all Storage domain exceptions. */
class StorageException extends BaseException
{
}
