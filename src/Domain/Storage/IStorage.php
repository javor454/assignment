<?php

declare(strict_types=1);

namespace Assignment\Domain\Storage;

use DateTimeImmutable;

/** CRUD interface for storages. */
interface IStorage
{
    /**
     * Reads value from the storage referenced by it's unique key identifier.
     *
     * @param mixed $key Unique identifier.
     * @param mixed $default Default value to return in case found value is expired or not found at all.
     * @return mixed Found valud.
     */
    public function read(string $key, $default = null);

    /**
     * Reads values from storage referenced by their unique key identifiers.
     *
     * @param string[] $keys Unique identifiers.
     * @param mixed $default Default value to return in case found values are expired or not found at all.
     * @return iterable Found values.
     */
    public function readMultiple(iterable $keys, $default = null): iterable;

    /**
     * Persists value in the storage, uniquely referenced by a key with an optional expiration time.
     *
     * @param string $key Unique identifier.
     * @param mixed $value The value to store.
     * @param \DateTimeImmutable|null $expiration Values time of expiration.
     * @return bool True on success and false on failure.
     */
    public function write(string $key, $value, ?DateTimeImmutable $expiration = null): bool;

    /**
     * Persists values in the storage, uniquely referenced by multiple keys with an optional expiration time.
     *
     * @param mixed[] $values The values to store.
     * @param \DateTimeImmutable|null $expiration Values time of expiration.
     * @return bool True on success and false on failure.
     */
    public function writeMultiple(iterable $values, ?DateTimeImmutable $expiration): bool;

    /**
     * Deletes value from the storage referenced by it's unique key.
     *
     * @param string $key Unique identifier for value.
     * @return bool True if the value was successfully removed. False if there was an error.
     */
    public function delete(string $key): bool;

    /**
     * Delete multiple values from the storage referenced by their unique keys.
     *
     * @param string[] $keys Unique identifiers for values.
     * @return bool True if the values were successfully removed. False if there was an error.
     */
    public function deleteMultiple(iterable $keys): bool;

    /**
     * Wipes clean the entire cache's keys.
     *
     * @return bool True on success and false on failure.
     */
    public function clear(): bool;

    /**
     * Determines whether an value referenced by a unique key identifier is present in the cache. Suitable for use outside
     * of storage/cache - paralel processes might remove the cache item marked as existing by this function.
     *
     * @param string $key Unique identifier for cache item.
     * @return bool True if there is an existing cache item found and false on failure.
     */
    public function exists(string $key): bool;

    /**
     * Returns all saved values in key value pairs e.g. `['P1' => 123]`.
     *
     * @return iterable
     */
    public function readAll(): iterable;
}
