<?php

declare(strict_types=1);

namespace Assignment\Domain\Driver;

/** Driver interface for Elastic Search. */
interface IElasticSearchDriver
{
    public function findById(string $id): array;
}
