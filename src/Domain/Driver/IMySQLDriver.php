<?php

declare(strict_types=1);

namespace Assignment\Domain\Driver;

/** Driver interface for MySQL. */
interface IMySQLDriver
{
    public function findProduct(string $id): array;
}
