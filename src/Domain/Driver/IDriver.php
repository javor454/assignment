<?php

declare(strict_types=1);

namespace Assignment\Domain\Driver;

/** Base driver interface, enforces unified way to retrieve product data in array by ID. */
interface IDriver
{
    /**
     * Finds product data by ID.
     *
     * @param string $id
     * @return array
     */
    public function findById(string $id): array;
}
