<?php

declare(strict_types=1);

namespace Assignment\Domain\Response;

/** Base response interface, enforces renderability. */
interface IResponse
{
    /**
     * Renders response.
     *
     * @return string
     */
    public function render(): string;
}
