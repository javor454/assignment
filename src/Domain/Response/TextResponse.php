<?php

declare(strict_types=1);

namespace Assignment\Domain\Response;

use Assignment\Domain\Exception\InvalidArgumentException;

/** Plain text response. */
class TextResponse implements IResponse
{
    private string $data;

    /**
     * Response contains plain text data formatted according to formating callback.
     *
     * @param mixed $data
     * @param callable $format
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function __construct($data, callable $format)
    {
        $text = $format($data);
        if (!is_string($text)) {
            throw new InvalidArgumentException('Formatting function did not return string.', ['data' => $data]);
        }
        $this->data = $text;
    }

    /**
     * Returns text.
     *
     * @return string
     */
    public function render(): string
    {
        return $this->data;
    }
}
