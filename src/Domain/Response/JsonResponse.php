<?php

declare(strict_types=1);

namespace Assignment\Domain\Response;

use Assignment\Domain\Exception\InvalidArgumentException;
use JsonException;

/** Validates and encapsulates data to be encoded to Json. */
class JsonResponse implements IResponse
{
    private string $data;

    /**
     * Encodes json serializable objects to json.
     *
     * @param mixed $data
     * @throws \Assignment\Domain\Exception\InvalidArgumentException
     */
    public function __construct($data)
    {
        try {
            $this->data = json_encode($data, JSON_THROW_ON_ERROR);
        } catch (JsonException $e) {
            throw new InvalidArgumentException($e->getMessage(), ['data' => $data]);
        }
    }

    /**
     * Returns json.
     *
     * @return string
     */
    public function render(): string
    {
        return $this->data;
    }
}
