<?php

declare(strict_types=1);

namespace Assignment\Domain\Util;

/** Utility methods for string manipulation. */
class StringHelper
{
    /**
     * Returns text with newline suffix.
     *
     * @param string $text
     * @return string
     */
    public static function addLine(string $text): string
    {
        return $text . PHP_EOL;
    }
}
