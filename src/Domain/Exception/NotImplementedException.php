<?php

declare(strict_types=1);

namespace Assignment\Domain\Exception;

/** Thrown to notify developers of unfinished features. */
class NotImplementedException extends BaseException
{
}
