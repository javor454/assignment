<?php

declare(strict_types=1);

namespace Assignment\Domain\Exception;

/** Exception for invalid arguments, which are passed on construct as array. */
class InvalidArgumentException extends BaseException
{
    /** @var mixed[] */
    private array $arguments;

    /**
     * Invalid arguments are passed as associative array e.g. `['key' => $key]`
     *
     * @param string $message
     * @param mixed[] $arguments
     */
    public function __construct(string $message, array $arguments)
    {
        $this->arguments = $arguments;

        $args = 'Arguments:' . PHP_EOL;
        foreach ($this->arguments as $name => $val) {
            $args .= $name . ' => ' . var_export($val, true) . PHP_EOL;
        }

        parent::__construct($message . PHP_EOL . $args);
    }

    /** @return mixed[] */
    public function getArguments(): array
    {
        return $this->arguments;
    }
}
