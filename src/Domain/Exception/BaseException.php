<?php

declare(strict_types=1);

namespace Assignment\Domain\Exception;

use Exception;

/** Base exception for Assignment project exceptions. */
class BaseException extends Exception
{
}
